Actores de "Avenger end game"

1-	Chris Evans como Steve Rogers / Capit�n Am�rica
2-	Robert Downey Jr. como Tony Stark / Iron Man
3-	Mark Ruffalo como Bruce Banner / Profesor Hulk
4-	Paul Rudd como Scott Lang / Ant-Man
5-	Chris Hemsworth como Thor
6-	Jeremy Renner como Clint Barton / Hawkeye / Ronin
7-	Brie Larson como Carol Danvers / Capitana Marvel
8-	Karen Gillan como Nebula
9-	Bradley Cooper como Rocket Raccoon
10-	Don Cheadle como James "Rhodey" Rhodes / M�quina de Guerra
11-	Scarlett Johansson como Natasha Romanoff / Black Widow
12-	Gwyneth Paltrow como Pepper Potts / Rescate
13-	Elizabeth Olsen como Wanda Maximoff / Scarlet Witch
14-	Benedict Cumberbatch como Dr. Stephen Strange
15-	Chris Pratt como Peter Quill / Star-Lord
16-	Zoe Salda�a como Gamora
17-	Tom Holland como Peter Parker / Spider-Man / Iron Spider
18-	Chadwick Boseman como T'Challa / Black Panther
19-	Anthony Mackie como Sam Wilson / Falcon
20-	Sebastian Stan como Bucky Barnes / Soldado del Invierno
21-	Tessa Thompson como Valquiria
22-	Benedict Wong como Wong
23-	Pom Klementieff como Mantis
24-	Evangeline Lilly como Hope van Dyne / Wasp
25-	Dave Bautista como Drax el Destructor
26-	Alexandra Rabe como Morgan Stark
27-	Ross Marquand como Johann Schimdt / Red Skull
28-	Vin Diesel como Groot
29-	Hayley Atwell como Peggy Carter
30-	Tom Hiddleston como Loki
31-	Letitia Wright como Shuri
32-	Jon Favreau como Happy Hogan
33-	Taika Waititi como Korg
34-	John Slattery como Howard Stark
35-	James D'Arcy como Edwin Jarvis
36-	Michael Douglas como Hank Pym
37-	Natalie Portman como Jane Foster
38-	Winston Duke como M'Baku
39-	Linda Cardellini como Laura Barton
40-	Robert Redford como Alexander Pierce

